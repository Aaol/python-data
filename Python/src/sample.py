import bonobo
import psycopg2
import requests

relationnal_connection_data = {
    "dbname":"irigo", 
    "user":"irigo", 
    "host":"localhost", 
    "password":"irigo", 
    "port":"35432"
}

def wait(any):
    return


# Fonction qui viennent récupérer les infos de l'API

def extract_buslines():
    yield from requests.get('https://data.angers.fr/api/records/1.0/search/?dataset=lignes-irigo&rows=-1&facet=ligne').json().get('records')
def extract_buslines_stop(any):
    yield from requests.get('https://data.angers.fr/api/records/1.0/search/?dataset=horaires-theoriques-et-arrets-du-reseau-irigo-gtfs&rows=-1&facet=stop_id&facet=stop_name').json().get('records')
def extract_buslines_dessert(any):
    yield from requests.get('https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-topologie-dessertes&rows=-1&facet=codeparcours&facet=mnemoligne&facet=nomligne&facet=dest&facet=mnemoarret&facet=nomarret&facet=numarret').json().get('records')


#Fonction de transformation des données

def transform_buslines(row):
    result =  {
        "ligne": row['fields']['ligne'],
        "date_maj": row ['fields']['date_maj'],
        "source": row ['fields']['source']
    }
    yield result

def transform_buslines_stop(row):
    print(row)
    result =  {
        "stop_id": row['fields']['stop_id'],
        "stop_name": row ['fields']['stop_name']
    }
    yield result

def transform_buslines_dessert(row):
    result =  {
        "iddesserte": row['fields']['iddesserte'],
        "idparcours": row ['fields']['idparcours'],
        "idarret": row ['fields']['idarret'],
        "mnemoligne": row ['fields']['mnemoligne'],
        "mnemoarret": row ['fields']['mnemoarret'],
        "nomarret": row ['fields']['nomarret'],
        "numarret": row ['fields']['numarret']
    }
    yield result 
#Fonction d'insertion de données

def insert_buslines(busline):
    with psycopg2.connect(**relationnal_connection_data) as conn:
        try:
            with conn.cursor() as cursor:
                cursor.execute("""INSERT INTO BusLine(ligne, date_maj, source) VALUES (%s, %s, %s)"""
                ,(busline['ligne']
                ,busline['date_maj']
                ,busline['source']))
        except:
            pass
    yield busline
def insert_buslines_stop(stop):
    with psycopg2.connect(**relationnal_connection_data) as conn:
        with conn.cursor() as cursor:
            cursor.execute("""INSERT INTO BusLineStop(stop_id, stop_name)VALUES (%s, %s)"""
            ,(stop['stop_id']
            ,stop['stop_name']))
 
    yield stop
def insert_buslines_dessert(busline_dessert):
    with psycopg2.connect(**relationnal_connection_data) as conn:
        try:
            with conn.cursor() as cursor:
                cursor.execute("""INSERT INTO BusLineBusLineStop(iddesserte, idparcours, idarret, mnemoligne, mnemoarret, nomarret, numarret) VALUES (%s, %s, %s, %s, %s, %s, %s)"""
                ,(busline_dessert['iddesserte']
                ,busline_dessert['idparcours']
                ,busline_dessert['idarret']
                ,busline_dessert['mnemoligne']
                ,busline_dessert['mnemoarret']
                ,busline_dessert['nomarret']
                ,busline_dessert['numarret']
                ))
        except:
            pass
    yield busline_dessert
def load(*args):
    """Placeholder, change, rename, remove... """
    print(*args)


def get_graph(**options):
    """
    This function builds the graph that needs to be executed.

    :return: bonobo.Graph

    """
    graph = bonobo.Graph()
    # graph.add_chain(
    #     bonobo.Limit(1),
    #     extract_buslines_dessert,
    #     transform_buslines_dessert,
    #     insert_buslines_dessert,
    #     _input=None
    #     )

    # graph.add_chain(extract_buslines, 
    #     transform_buslines,
    #     insert_buslines,
    #     _output=extract_buslines_dessert)
    # graph.add_chain(
    #     extract_buslines_stop, 
    #     transform_buslines_stop,
    #     insert_buslines_stop,
    #     _output=extract_buslines_dessert)
    graph.add_chain(
        extract_buslines, 
        transform_buslines,
        insert_buslines,
        bonobo.Limit(1),
        extract_buslines_stop, 
        transform_buslines_stop,
        insert_buslines_stop,
        bonobo.Limit(2),
        extract_buslines_dessert,
        transform_buslines_dessert,
        insert_buslines_dessert
        )

    # graph.add_chain(
    #     _output=extract_buslines_dessert)
    # graph.add_chain(
    #     _output=extract_buslines_dessert)

    return graph


def get_services(**options):
    """
    This function builds the services dictionary, which is a simple dict of names-to-implementation used by bonobo
    for runtime injection.

    It will be used on top of the defaults provided by bonobo (fs, http, ...). You can override those defaults, or just
    let the framework define them. You can also define your own services and naming is up to you.

    :return: dict
    """
    return {}

def f():
    print("hello")

# The __main__ block actually execute the graph.
graph = get_graph()
bonobo.run(graph)