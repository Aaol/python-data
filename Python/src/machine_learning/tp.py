import bonobo
import csv
# Définition d'une fonction qui vient extraire la données que l'on veut afficher si elle est non nulle
def extract_value_from_index(value, index, field_class):
    if(index < len(value)):
        return {"value": value[index], "class": field_class}
    else:
        return None
    
# Fonction où l'on fourni l'objet que l'on veut traiter, et la liste des classes associées à chaque colonne
# Vient récupérer les chaines de caractères et les associe à la classe, en procédant à un nettoyage des valeurs nulles
def extract_values_from_classlist(value, class_list):
    result = []
    for i in range(1, len(class_list)):
        result.append(extract_value_from_index(value, i, class_list[i]))
        
    return [x for x in result if x is not None or x != '']

    
# Définition des fonctions de transformations de données
def transform_telecom_values(value):
    classlist=[10, 10, 3, 3, 10, 10, 10, 3]
    yield from extract_values_from_classlist(value, classlist)

def transform_network_values(value):
    classlist=[7, 10, 10, 10, 10, 10, 10, 10]
    yield from extract_values_from_classlist(value, classlist)
    
def transform_sirc_values(value):
    classlist=[8, 10, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 10
               , 10, 10, 4, 2, 2, 10, 4 ,10, 10, 10, 10, 4, 10, 10, 10, 10, 10, 10
               , 10, 10, 10, 10, 7 ,10 , 10, 9, 6, 10,10,10,6,10
               ,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,9,10
               ,6,10,10,10,10,10,10,6,10,7,7,10,10,10,10,10,10,10,10,10,10,10
               ,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,5,11]
    yield from extract_values_from_classlist(value, classlist)
    
def transform_osb_values(value):
    classlist=[10, 10, 10, 10, 6, 6, 8, 10, 10, 10, 2, 4, 11, 10, 12, 4, 10, 10, 10, 3, 10, 10, 10, 10, 10, 10, 4, 9]
    yield from extract_values_from_classlist(value, classlist)
def transform_intercite_values(value):
    classlist=[10, 7, 4, 4, 1, 1, 1, 1, 1]
    yield from extract_values_from_classlist(value, classlist)

def read_from_csv(path):
    with open(path, 'rt', encoding='utf-8') as csvfile:
        rowreader = csv.reader(csvfile, delimiter=';')
        #On passe à la ligne suivant pour ne pas récupérer la ligne de description des colonnes
        next(rowreader, None)
        yield from rowreader
#Définition du graphe bonobo
graph = bonobo.Graph()

#Définition de l'élément qui vient écrire dans le fichier data.csv
graph.add_chain(
    bonobo.UnpackItems(0)
    , bonobo.CsvWriter('data.csv')
    , _input=None
    , _name="write")

#Chargement fichers csv

graph.add_chain(bonobo.CsvReader('regularite-mensuelle-intercites.csv')
                , transform_intercite_values
                , _output="write")
graph.add_chain(read_from_csv('obs.csv')
                , transform_osb_values
                , _output="write"
                )
graph.add_chain(read_from_csv('sirc-17804_9075_61171_2018102_E_Q_20180413_015943916.csv')
                , transform_sirc_values
                , _output="write")

graph.add_chain(read_from_csv('telecom_qualite_services_mobiles_07-2016.csv')
                , transform_telecom_values
                , _output="write")

graph.add_chain(read_from_csv('validations-sur-le-reseau-de-surface-nombre-de-validations-par-jour-1er-semestr0.csv')
                , transform_network_values
                , _output="write")
r = bonobo.run(graph)