import bonobo
import requests
import psycopg2
import redis

relationnal_connection_data = {
    "dbname":"irigo", 
    "user":"irigo", 
    "host":"localhost", 
    "password":"irigo", 
    "port":"35432"
}

tile38_connection_data = {
    "host": '127.0.0.1', 
    "port": '39851'
}


ROWS = 10000
# Fonctions de récupération des données
def fetch_desserte_data():
    url = f"https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-topologie-dessertes&rows={ROWS}"
    response = requests.get(url)
    for record in response.json().get("records"):
        yield record



def get_interesting_fields(record):
    result = {
        "line_id": record["fields"]["mnemoligne"],
        "stop_id": record["fields"]["mnemoarret"],
        "line_name": record["fields"]["nomligne"],
        "stop_name": record["fields"]["nomarret"],
        "stop_lon": record["fields"]["coordonnees"][1],
        "stop_lat": record["fields"]["coordonnees"][0],
        "desserte_id": record["fields"]["iddesserte"]
    }
    yield result

def insert_in_tile38(record):
    """See https://github.com/tidwall/tile38
    stop_id, lon, lat, radius
    """
    client = redis.Redis(**tile38_connection_data)
    collection_name = "mycollection"
    channel_name ="mychan"
    cmd = [
        "SETCHAN",
        channel_name,
        "nearby",
        collection_name,
        "fence",
        "point",
        record["stop_lon"],
        record["stop_lat"],
        "200"
    ]
    result = client.execute_command(*cmd)
    print(record)
    yield record
    
def insert_in_relationnal(record):
    with psycopg2.connect(**relationnal_connection_data) as connection:
        with connection.cursor() as cursor:
            try:
                # insert line data in DB
                cursor.execute("""
                INSERT INTO BusLine (id, ligne) VALUES (%s, %s) ON CONFLICT (ligne) DO UPDATE SET id=EXCLUDED.id;
                """, (record["line_id"], record["line_name"]))
            except psycopg2.IntegrityError as e:
                # print("line already exists", record["line_id"])
                pass
    with psycopg2.connect(**relationnal_connection_data) as connection:
        with connection.cursor() as cursor:
            try:
                # insert stop data in DB
                data = (
                    record["stop_id"],
                    record["stop_name"],
                    record["stop_lon"],
                    record["stop_lat"]
                )
                cursor.execute("""
                INSERT INTO BusLineStop (stop_id, stop_name, lon, lat) VALUES (%s, %s, %s, %s)  ON CONFLICT (stop_id) DO UPDATE SET stop_name=EXCLUDED.stop_name, lon=EXCLUDED.lon, lat=EXCLUDED.lat;
                """, data)
            except psycopg2.IntegrityError as e:
                #print("stop already exists", record["stop_id"])
                pass
    with psycopg2.connect(**relationnal_connection_data) as connection:
        with connection.cursor() as cursor:
            try:
                # insert desserte data in DB
                data = (
                    record["desserte_id"],
                    record["stop_id"],
                    record["line_name"]
                )
                cursor.execute("""
                INSERT INTO BusLineBusLineStop (iddesserte, mnemoarret, mnemoligne) VALUES (%s, %s, %s)  ON CONFLICT (iddesserte) DO UPDATE SET mnemoligne=EXCLUDED.mnemoligne, mnemoarret=EXCLUDED.mnemoarret;
                """, data)
            except psycopg2.IntegrityError as e:
                # print("desserte already exists", record["desserte_id"])
                pass
    yield record

graph = bonobo.Graph()
graph.add_chain(
    fetch_desserte_data, 
    get_interesting_fields,
    insert_in_relationnal
)
graph.add_chain(
    insert_in_tile38,
    _input=get_interesting_fields
)
r = bonobo.run(graph)

