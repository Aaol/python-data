import redis
import bonobo
import psycopg2
import requests
import json
import time
connexion_config = {}
with open("./config.json","rt") as f:
    connexion_config = json.load(f) 
relationnal_connection_data = {
    "dbname":"irigo", 
    "user":"irigo", 
    "host":"localhost", 
    "password":"irigo", 
    "port":"35432"
}

tile38_connection_data = {
    "host": '127.0.0.1', 
    "port": '39851'
}

ROWS = 100

def fetch_vehicle_data():
    for i in range(30):
        url = f"https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-position-tr&rows={ROWS}"
        response = requests.get(url)
        for record in response.json().get("records"):
            yield record
        time.sleep(30)

def insert_timeserie(record):
    
    with psycopg2.connect(**relationnal_connection_data) as connection:
        with connection.cursor() as cursor:
            try:
                # insert stop data in DB
                data = (
                    record['fields']['idvh'],
                    record['fields']['coordonnees'][0],
                    record['fields']['coordonnees'][1],
                    record['fields']['type'],
                    record['fields']['etat'],
                    record['fields']['iddesserte'],
                    "NOW()",
                    record['fields']['harret'],
                    record['fields']['ecart']
                )
                cursor.execute("""
                INSERT INTO vehicle (idvh, lon, lat, type, etat, iddesserte, time, hanet, ecart) VALUES (%s, %s, %s, %s, %s, %s,%s,%s, %s) ;
                """, data)
            except psycopg2.IntegrityError as e:
                #print("stop already exists", record["stop_id"])
                pass
    yield record
def insert_tile38(record):
    client = redis.Redis(**connexion_config.get('tile38_connection_data'))
    id=record['fields']['idvh'],
    lat=record['fields']['coordonnees'][1]
    lon=record['fields']['coordonnees'][0]

    collection_name = "mycollection"
    cmd = [
        "SET",
        collection_name,
        f"vehicule {id}",
        "point",
        lat,
        lon
    ]
    result = client.execute_command(*cmd)
    print(f"vehicule {id}" + f"deplacement en {lat} {lon}" )
    yield record

graph = bonobo.Graph()

graph.add_chain(fetch_vehicle_data)
graph.add_chain(insert_timeserie, _input=fetch_vehicle_data)
graph.add_chain(insert_tile38, _input=fetch_vehicle_data)
r = bonobo.run(graph)
