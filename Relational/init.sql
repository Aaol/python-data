CREATE TABLE BusLine
(
    ligne VARCHAR(200) PRIMARY KEY,
    id VARCHAR(500)
);

CREATE TABLE BusLineStop
(
    stop_id VARCHAR(200) PRIMARY KEY,
    stop_name VARCHAR(500),
    lon REAL,
    lat REAL

);

CrEATE TABLE BusLineBusLineStop
(
    iddesserte INT PRIMARY KEY,
    idparcours INT,
    idarret INT,
    mnemoligne VARCHAR(200),
    mnemoarret VARCHAR(200),
    nomarret VARCHAR(200),
    numarret int,
    CONSTRAINT FK_BusLine FOREIGN KEY (mnemoligne)
        REFERENCES BusLine(ligne),
    CONSTRAINT FK_BusLineStop FOREIGN KEY (mnemoarret)
        REFERENCES BusLineStop(stop_id)
);


CREATE TABLE vehicle (
    idvh int,
    lon real,
    lat real,
    type varchar(200),
    etat varchar(200),
    iddesserte int,
    time        TIMESTAMPTZ       NOT NULL,
    hanet date,
    ecart int
);

SELECT create_hypertable('vehicle', 'time');